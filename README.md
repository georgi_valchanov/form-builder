# Dynamic Form Builder

## Deployed at:
[GitLabs](https://georgi_valchanov.gitlab.io/form-builder/)

## Supported Features

- Supported Input types
  - Input
  - Email
  - Checkbox
  - Options
- Reacting to html form validation
  - Submission prevention
  - Feed back messages

## Usage

The component takes in an array of form element descriptors with the following structure:

    {
        "id": "firstname",
        "label": "First Name",
        "placeholder": "Enter your first name ...",
        "type": "text" | "email" | "select" | "checkbox", // type of the input field
        "value": "", //default value
        "validity": { // the keys of the object are HTML input properties describe here https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation#using_built-in_form_validation
            "required": true,
            "minLength": 2,
            "maxLength": 20,
            "pattern": "RegEx"
        },
        "validityStateMsg": { //keys of this object match this object https://developer.mozilla.org/en-US/docs/Web/API/ValidityState 
            //provide custom messages for a given type of invalidity
            "tooShort": "Your First Name cannot be shorter than 2 characters."
        },
        "visibility": { //provides visibility definition (ie field is visible depending on a condition)
          "dependentProperty": "subscribe", // id of a property on which the visibility is dependent 
          "expectedValue": true,            //value to match so that visibility is triggered
          "negation": true                 //can be used to have visibility = !(subscribe === true) 
        },
        "options": [ // in case of a Dropdown(select) input field
            {
                "value": string, 
                "label": string 
            }
        ],
    }
