import React from "react";
import FormElementWrapper from "./FormElementWrapper";
import PropTypes from "prop-types";
import commonPropTypes from "./CommonPropTypes";

function SelectFormElement(props) {
  const options = [
    { value: "", label: "--please select a value--", isDisabled: true },
    ...props.options
  ];

  const onChangeHandler = ({ target }) => {
    props.onChange(target.value, target.id, target);
  };

  return (
    <FormElementWrapper
      formInputId={props.id}
      labelText={props.label}
      isRequired={props.validity?.required}
      invalidityFeedback={props.invalidityText}
    >
      <select
        id={props.id}
        name={props.id}
        defaultValue={""}
        required={props.validity?.required}
        onChange={onChangeHandler}
        className="form-control"
      >
        {options.map((o) => (
          <option key={o.value} value={o.value} disabled={o.isDisabled}>
            {o.label}
          </option>
        ))}
      </select>
    </FormElementWrapper>
  );
}

SelectFormElement.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ),
  ...commonPropTypes
};

export default SelectFormElement;
