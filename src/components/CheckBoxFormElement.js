import React from "react";
import FormElementWrapper from "./FormElementWrapper";
import commonPropTypes from "./CommonPropTypes";

function CheckBoxFormElement(props) {
  const onChangeHandler = ({ target }) => {
    props.onChange(target.checked, target.id, target);
  };
  return (
    <FormElementWrapper
      formInputId={props.id}
      labelText={props.label}
      isRequired={props.validity?.required}
      invalidityFeedback={props.invalidityText}
      inputIsCheckbox={true}
    >
      <input
        type="checkbox"
        id={props.id}
        name={props.id}
        required={props.required}
        value={props.value}
        className="form-check-input"
        onChange={onChangeHandler}
      />
    </FormElementWrapper>
  );
}

CheckBoxFormElement.propTypes = commonPropTypes;
export default CheckBoxFormElement;
