export default class FormValidityWrapper {
  constructor(validityMessages) {
    this.validityMessages = validityMessages;
    this.lastValidationResults = new Map();
  }

  processAndRememberElementValidityState(
    elementName,
    { validity, validationMessage }
  ) {
    let invalidityText = validationMessage;
    const customValidationMessages = this.validityMessages.get(elementName);

    if (customValidationMessages) {
      for (let key in validity) {
        if (key !== "valid" && validity[key]) {
          invalidityText = customValidationMessages[key] || invalidityText;
          break;
        }
      }
    }

    const result = {
      invalidityText,
      isValid: validity.valid
    };

    this.lastValidationResults.set(elementName, result);
    return result;
  }

  isFormValid(HTMLForm) {
    return [...HTMLForm.elements]
      .filter((f) => f.type !== "submit")
      .reduce(
        (validityResult, formField) => {
          const { isValid, invalidityText } =
            this.processAndRememberElementValidityState(formField.name, formField);
          validityResult.isValid = validityResult.isValid && isValid;
          validityResult.invalidityMessages[formField.name] = invalidityText;
          return validityResult;
        },
        {
          isValid: true,
          invalidityMessages: {}
        }
      );
  }

  getLastValidationResult(elementName) {
    const validationResult = this.lastValidationResults.get(elementName);
    return (
      validationResult || {
        isValid: true,
        invalidityText: null
      }
    );
  }
}
