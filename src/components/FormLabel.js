import React from "react";
import PropTypes from "prop-types";

function FormLabel(props) {
  const cssClasses = ["form-label"];
  if (props.isCheckBoxLabel) {
    cssClasses.push("form-check-label");
  }
  return (
    <label htmlFor={props.forId} className={cssClasses.join(" ")}>
      <span>{props.text}</span>
      {props.required && <span style={{ color: "red" }}>*</span>}
    </label>
  );
}

FormLabel.propTypes = {
  forId: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isCheckBoxLabel: PropTypes.bool,
  required: PropTypes.bool
};

export default FormLabel;
