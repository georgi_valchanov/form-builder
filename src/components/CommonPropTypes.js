import PropTypes from "prop-types";

const commonPropTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),

  validity: PropTypes.shape({
    required: PropTypes.bool,
    minLength: PropTypes.number,
    maxLength: PropTypes.number,
    pattern: PropTypes.string
  }),
  validityStateMsg: PropTypes.shape({
    valueMissing: PropTypes.string,
    typeMismatch: PropTypes.string,
    tooLong: PropTypes.string,
    stepMismatch: PropTypes.string,
    rangeUnderflow: PropTypes.string,
    rangeOverflow: PropTypes.string,
    patternMismatch: PropTypes.string,
    badInput: PropTypes.string
  })
};

export default commonPropTypes;
