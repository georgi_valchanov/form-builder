import React from "react";
import FormElementWrapper from "./FormElementWrapper";
import commonPropTypes from "./CommonPropTypes";

function TextFormElement(props) {
  if (!["text", "email"].includes(props.type)) {
    throw new Error("Text form element can only be of type text or email");
  }

  const onChangeHandler = ({ target }) => {
    props.onChange(target.value, target.id, target);
  };

  return (
    <FormElementWrapper
      formInputId={props.id}
      labelText={props.label}
      isRequired={props.validity?.required}
      invalidityFeedback={props.invalidityText}
    >
      <input
        id={props.id}
        name={props.id}
        type={props.type}
        placeholder={props.placeholder}
        value={props.value}
        className="form-control"
        onChange={onChangeHandler}
        required={props.validity?.required}
        maxLength={props.validity?.maxLength}
        minLength={props.validity?.minLength}
        pattern={props.validity?.pattern}
      />
    </FormElementWrapper>
  );
}

TextFormElement.propTypes = commonPropTypes;
export default TextFormElement;
