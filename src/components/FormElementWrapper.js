import React from "react";
import FormLabel from "./FormLabel";
import PropTypes from "prop-types";

function FormElementWrapper({
  formInputId,
  inputIsCheckbox = false,
  labelText,
  isRequired = false,
  invalidityFeedback,
  children
}) {
  return (
    <div className={inputIsCheckbox ? "mb-3 form-check" : "mb-3"}>
      {Boolean(inputIsCheckbox) && children}
      <FormLabel
        forId={formInputId}
        text={labelText}
        required={isRequired}
        isCheckBoxLabel={inputIsCheckbox}
      />
      {!inputIsCheckbox && children}
      {invalidityFeedback && (
        <div className="invalid-feedback">{invalidityFeedback}</div>
      )}
    </div>
  );
}

FormElementWrapper.propTypes = {
  formInputId: PropTypes.string.isRequired,
  inputIsCheckbox: PropTypes.bool,
  labelText: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  invalidityFeedback: PropTypes.string,
  children: PropTypes.any
};

export default FormElementWrapper;
