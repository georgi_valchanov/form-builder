import React from "react";
import TextFormElement from "./TextFormElement";
import SelectFormElement from "./SelectFormElement";
import CheckBoxFormElement from "./CheckBoxFormElement";
import FormValidityWrapper from "./FormValidityWrapper";

import PropTypes from "prop-types";

const unsupportedInputStyle = {
  backgroundColor: "red",
  color: "white",
  padding: ".5rem"
};

class DynamicFormBuilder extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.initState(props);
    this.formValidityWrapper = this.initValidityWrapper(props);

    this.onValueChange = this.onValueChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  initState(props) {
    const formFieldValues = props.fields.reduce((formValueObj, formFieldDesc) => {
      formValueObj[formFieldDesc.id] = formFieldDesc.value || "";
      return formValueObj;
    }, {});

    return {
      wasValidated: false,
      formFieldValues
    };
  }

  initValidityWrapper(props) {
    const validityMessages = props.fields.reduce((result, current) => {
      result.set(current.id, current.validityStateMsg);
      return result;
    }, new Map());

    return new FormValidityWrapper(validityMessages);
  }

  onValueChange(value, propName, { validity, validationMessage }) {
    this.formValidityWrapper.processAndRememberElementValidityState(propName, {
      validity,
      validationMessage
    });
    const newState = { ...this.state };
    newState.formFieldValues[propName] = value;
    this.setState(newState);
  }

  onSubmit(event) {
    const newState = { ...this.state, wasValidated: true };

    const validityResult = this.formValidityWrapper.isFormValid(event.target);

    if (validityResult.isValid) {
      this.props.onSubmit({ ...this.state.formFieldValues });
    }
    this.setState(newState);
    event.preventDefault();
  }

  checkVisibility(formFieldDef) {
    if (!formFieldDef.visibility) {
      return true;
    }
    const { dependentProperty, expectedValue, negation } = formFieldDef.visibility;
    return Boolean(
      negation ^
        Number(this.state.formFieldValues[dependentProperty] === expectedValue)
    );
  }

  renderFormElementBasedOnType(formFieldDef) {
    const { type, id } = formFieldDef;
    const formProps = {
      key: id,
      onChange: this.onValueChange,
      ...formFieldDef,
      value: this.state.formFieldValues[id],
      ...this.formValidityWrapper.getLastValidationResult(formFieldDef.id)
    };

    switch (type) {
      case "text":
      case "email":
        return <TextFormElement {...formProps} />;
      case "select":
        return <SelectFormElement {...formProps} />;
      case "checkbox":
        return <CheckBoxFormElement {...formProps} />;
      default:
        return (
          <p
            key={id}
            style={unsupportedInputStyle}
          >{`Field with id: ${id} has unsupported type: ${type}`}</p>
        );
    }
  }

  render() {
    return (
      <form
        onSubmit={this.onSubmit}
        noValidate
        className={this.state.wasValidated ? "was-validated" : ""}
      >
        {this.props.fields
          .filter(this.checkVisibility.bind(this))
          .map(this.renderFormElementBasedOnType.bind(this))}
        <input type="submit" name="submit" className="btn btn-primary" />
      </form>
    );
  }
}

DynamicFormBuilder.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      ...TextFormElement.propTypes,
      ...SelectFormElement.propTypes,
      ...CheckBoxFormElement.propTypes
    })
  ).isRequired,
  onSubmit: PropTypes.func
};

export default DynamicFormBuilder;
