import { hot } from "react-hot-loader/root";
import formJSON from "./formData.json";
import DynamicFormBuilder from "./components/DynamicFormBuilder";
import "./App.css";
import React from "react";

function App() {
  return (
    <div className="container">
      <DynamicFormBuilder
        fields={formJSON}
        onSubmit={(formData) => console.log(formData)}
      />
    </div>
  );
}

export default hot(App);
